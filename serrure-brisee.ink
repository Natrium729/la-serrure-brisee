# title: La Serrure brisée
# author: Nathanaël Marion

/* Après le nom de chaque nœud et maille se trouve le nombre de mots qu’il contient.

(Il y a de bonnes chances que je me sois trompé de quelques mots, mais on n’est pas à ça prêt, tant qu’on reste en dessous des 3000 !)

Nombre de mots total :
— Selon mon comptage : 2134 ;
— Selon Antidote : 2302.

J’ai dû faire une erreur quelque part. Tant pis ! Antidote doit être plus proche de la réalité. */


// Mettre cette variable à vrai quand on teste le jeu.
VAR TEST = true

// Les variables

VAR plan = false
VAR est_perdue = 0

// On va vers les tests ou le début du jeu.

{TEST: -> menu_test|-> introduction}

=== menu_test ===

/* Ce menu n’est évidemment pas disponible dans la version finale, quand la variable TEST est à faux. */

*	Commencer le jeu normalement.
	-> introduction

*	Tester les couloirs avec le plan.
	~ plan = true
	-> couloirs

*	Tester les couloirs sans le plan.
	-> couloirs

=== introduction // 35 mots.

Des coups rapides sont frappés à la porte. Agacée, Aoda lève les yeux de ses documents et hésite. Elle n’aime pas être dérangée dans son travail.

*	Elle répond[.] :
	-> repondre

*	Elle se replonge dans sa paperasse.
	-> paperasse

= paperasse // 78 mots.

Des colonnes et des colonnes pour le nom, la provenance et le temps passé dans son établissement pour chacun de ses clients. Des informations précieuses, qu’Aoda aime vérifier au calme. Mais pour le moment, les coups reprennent, plus pressants. La poignée joue, mais la porte est verrouillée, et une voix s’élève :

« Aoda, ouvre, on a besoin de toi ! C’est assez urgent ! »

Si ce n’est qu’<i>assez</i> urgent…

*	Elle ignore l’importun[.] et se replonge dans ses documents.
	-> ignorer

*	Elle répond à contrecœur[.] :
	-> repondre

= ignorer // 38 mots.

/* Pour une certaine raison, le compteur d’un passage ne s’incrémente pas si on y rentre depuis lui-même. J’utilise donc un point de rassemblement nommé pour pouvoir compter le nombre de passages dans ce nœud.

J’aurais pu utiliser une variable, mais l’avantage ici, c’est que le compteur est appelé « ignorer.encore », et j’aime bien. */

-	(encore) {~On continue de frapper|Les coups redoublent|L’autre insiste}, elle {~fronce les sourcils|s’efforce d’ignorer|fait comme si de rien n’était}.

+	Elle persiste et garde les yeux sur son document.
	-> encore

*	Elle soupire et répond[.] :
	-> repondre

= repondre // 100 mots

« Qu’y a-t-il ?

— La porte menant au couloir, celui qui… enfin, tu vois lequel, quoi. Ben elle ne s’ouvre plus !

— Impossible ! rétorque Aoda. Elle n’est jamais verrouillée, pour justement éviter ce genre de problème.

— Je sais… Mais tu n’as qu’à vérifier par toi-même. Sinon, t’auras qu’à t’expliquer avec ceux qui sont bloqués. On n’aura pas dit que je ne t’aurais pas prévenue. »

L’homme derrière la porte s’éloigne. Aoda soupire{ignorer: de nouveau}, se saisit de l’imposant trousseau qui ne la quitte jamais, se lève et quitte son bureau.

-> devant_la_porte

=== devant_la_porte === // 50 mots

TODO: Effacer l’écran ou ajouter une petite transition ? (Comme un fondu.)

Après avoir traversé de nombreux couloirs aux papiers peints de couleurs différentes et emprunté d’abrupts escaliers, elle arrive aux tréfonds de l’hôtel.

Dans la pénombre, la fameuse porte est là, devant elle. Aoda essaie d’ouvrir, en vain ; le battant refuse de bouger. Ce n’est pas normal.

-> ouvrir

= ouvrir  // 252 mots.

*	Elle [regarde par la serrure] pose un genou à terre et regarde par la serrure.

	Comme elle s’y attendait, le noir complet. Cette partie du bâtiment n’est pas éclairée.

*	Elle cherche la clef.

	Plissant les yeux pour trouver la bonne dans la semi-obscurité, elle fait défiler l’énorme trousseau. Quelques minutes plus tard, elle insère la clef dans la serrure, la tourne. Tente de la tourner, en fait. La serrure serait-elle brisée ?

*	Elle décide d’utiliser la manière forte.

	La subtilité, elle la laisse à Céleste. Elle recule de quelques pas et se jette sur le battant, qui ne plie pas. Aoda étouffe un juron comme des pas se font entendre, suivi d’une voix.

	« Inutile, on a déjà essayé. C’est cette décision d’Aoda d’avoir des portes inviolables…

	— Ah. »

	Reconnaissant ce « ah » caractéristique de la propriétaire de l’hôtel, l’individu derrière la porte marmonne des excuses sous les gloussements des autres qui sont bloqués avec lui.

	Avec un sourire un tantinet sardonique, elle ajoute :

	« Continuez de tenter de trouver un moyen de sortir, je vais voir de mon côté. »

*	->
	Elle n’a plus rien à faire et revient sur ses pas. Il lui faut trouver autre chose. Elle pourrait appeler un serrurier mais il risquerait alors de découvrir le secret de l’hôtel. Elle est en revanche certaine qu’il y avait une autre issue, qui a été condamnée longtemps auparavant, et elle se dirige vers les archives dans l’espoir de trouver plus d’informations.

	-> archives

-	-> ouvrir

=== archives === // 63 mots.

Après avoir retraversé l’hôtel, Aoda pénètre dans une pièce remplie de cartons et d’objets recouverts par des draps. De vieux souvenirs, des cadeaux de clients — surtout pour Céleste, il est vrai — et, parmi tout ça, d’antiques papiers. Un jour, peut-être, Aoda trouvera le temps de tout trier.

Mais pas maintenant. Un ancien plan du manoir doit se trouver quelque part.

- (options)

<- boites
<- etageres
<- partir

-> DONE

= boites // 91 mots.

*	Elle fouille méthodiquement les boîtes.

	Soudain, elle tombe sur un morceau de papier jauni. Pressentant qu’elle a trouvé ce pour quoi elle était venue, elle le déplie.

	Un plan d’une section de l’hôtel. En l’examinant attentivement, on peut apercevoir un escalier avec pour légende « escalier perdu », menant à un sous-sol relié à la pièce dont la porte est bloquée.

	Sauf qu’Aoda en est certaine, cet escalier n’existe pas… Tant pis pour le moment, elle empoche le plan, contente d’avoir trouvé quelque chose aussi rapidement.

	~ plan = true

	-> options

= etageres // 39 mots.

*	Elle regarde les étagères poussiéreuses.

	Rapidement, elle inspecte les piles de feuilles qui ont été entreposées là. Elle a l’habitude des documents, elle les survole et sait bientôt qu’il n’y a rien qui l’intéresse ici.

	-> options

= partir

*	Elle quitte la pièce.

	{
	- plan:
		Elle doute de la validité de la carte, mais elle n’a pas d’autre piste de toute façon, et elle ne peut pas perdre plus de temps.
	- else:
		Quand elle a vu la montagne de cartons qui l’attendaient, elle s’est dit qu’elle n’avait pas de temps à perdre en vaines recherches. Elle se fiera à son instinct.
	}

	-> couloirs

=== pensees === // 156 mots.

/*
Ces passages s’affichent à la suite, un à chaque « tour » passés dans les couloirs.
*/

{once:

	- Elle se met à réfléchir. Il n’empêche que cette porte n’aurait jamais dû être verrouillée et sa serrure n’aurait jamais dû être cassée.

	- Quel affront, tout de même ! Elle a fondé cet hôtel dans le manoir de ses ancêtres. L’établissement, le plus grand du monde et d’au-delà, se doit d’être irréprochable et l’a toujours été.

	- Ça aurait été grave — une attaque de Réviseurs par exemple —, elle l’aurait accepté. Mais là, tous ses clients, venant de partout et de très très loin, coincés à cause d’une simple serrure cassée ; on ne peut imaginer plus stupide. La honte, vraiment !

	- Qui a bien pu verrouiller la porte ? Il n’y a que Céleste et elle qui en possèdent la clef. Mais son amie, qui a fondé l’hôtel avec elle, est exempte de tout soupçon.

	- Elle chasse ses pensées parasites. Elle devrait d’abord s’occuper du problème immédiat. Elle résoudra ce mystère plus tard.
}

->->

=== couloirs === // 14 mots.

Elle se met à arpenter le manoir, dont elle connaît les couloirs par cœur.

-> perdue

= perdue // 0 mot, ha ha.

/* Ce passage décide quelle sera la bonne option parmi les quatre qui seront proposées, puis on récupère chaque option avec le nœud « chemin » qui affichera ce qu’il faut grâce à son argument.

Bien sûr, comme le but est en vérité de se perdre, la « bonne » option est en fait celle qu’il ne faut pas choisir ! :) */

~ temp bonne_option = RANDOM(1, 4)

{TEST: {est_perdue}}

<- chemin(bonne_option == 1)
<- chemin(bonne_option == 2)
<- chemin(bonne_option == 3)
<- chemin(bonne_option == 4)

-> DONE

= chemin(bon) // 228 mots.

/* Ici, c’est un peu difficile de compter les mots puisque les phrases sont construites aléatoirement. J’ai décidé de compter chaque morceau de phrase indépendamment, et pas de faire la liste de toutes les phrases possibles et de compter les mots après (en espérant que c’est clair). */

/* Ci-dessous, on stocke le texte de l’option dans une variable. Comme ça, on ne l’évalue qu’une seule fois.

Si on met le texte variable directement dans l’option, alors il sera évalué une première fois quand l’option est affichée, et une deuxième fois quand l’option est choisie, ce qui n’affiche alors pas le même texte.

Et si le début de la phrase n’est pas inclus dans la variable, c’est parce qu’on ne peut pas afficher une variable au début d’une option, sinon ink croit qu’il s’agit d’une condition. */

~ temp texte_option = ""

{
- bon and plan:
	~ texte_option = "il faudrait {~tourner à gauche|tourner à droite|continuer tout droit} pour {~suivre le bon chemin|arriver à l’escalier perdu|atteindre le passage secret}"

- bon and not plan:
	~ texte_option = "{~sa mémoire est bonne|elle se souvient bien|ses déductions sont correctes}, alors {~elle a des chances de trouver quelque chose en empruntant ce couloir|passer par ici devrait l’approcher du but|elle doit utiliser ce passage}"

- not bon:
	~ texte_option = "{~décide|choisis} {~de prendre|d’emprunter|de suivre} {~ce qui lui semble être un raccourci|ce passage latéral|un couloir sur le côté}"
}

+	{bon and plan} Selon le plan, {texte_option}.

	{~Confiante|Sûre d’elle|Déterminée}, Aoda {~avance|marche d’un bon pas|poursuit son avancée}.

	{
	- est_perdue > 0:
		~ est_perdue--
	}

	-> pensees ->

	{&|||||Cela fait longtemps qu’elle chemine ainsi. Peut-être que ce plan n’indique pas vraiment l’emplacement de cet escalier <i>perdu</i> ?}

+	{bon and not plan} Si {texte_option}.

	{~Son intuition la guidant|Sentant qu’elle est sur la bonne voie|Se fiant à son sixième sens}, elle continue {~sans se poser de questions|d’un pas assuré|sans l’ombre d’une hésitation}.

	{
	- est_perdue > 0:
		~ est_perdue--
	}

	-> pensees ->

	{&|||||Cela fait longtemps qu’elle chemine ainsi. Son instinct la tromperait-elle ?}

+	{not bon} Elle {texte_option}.

	-> pensees ->

	~ est_perdue++

	{
	- est_perdue == 5:
		Pendant cette marche interminable, Aoda se sent de plus en plus désorientée. Se serait-elle perdue ? Impossible, elle a parcouru des centaines de fois le bâtiment. Et pourtant, elle atteint bien un escalier qu’elle n’a jamais vu, ne retenant qu’un vague souvenir seulement de ses déambulations dans l’hôtel.

		Elle descend.

		-> passage_secret
	}

-	-> perdue

=== passage_secret === // 91 mots.

De nombreuses marches plus bas, la lumière diminue et Aoda finit par atteindre une porte. Fébrilement, elle prend son trousseau et essaie les clefs une à une. Il y en a beaucoup et elle perd beaucoup de temps avant de se rendre compte que la porte n’est pas verrouillée.

Se réprimandant à mi-voix, elle ouvre, découvrant une galerie de pierre. À peine a-t-elle franchi le seuil que la porte se referme, plongeant l’endroit dans l’obscurité la plus complète.

*	Soudain prise d’une angoisse irrationnelle, elle [ressort.]tente de ressortir.

	Elle tourne frénétiquement la poignée, mais rien à faire, la porte reste close. Elle va commencer à croire que toutes les serrures ont un problème dans ce fichu hôtel ! Elle se force à respirer pour se calmer, fait volte-face et se met à avancer, une main suivant le mur de droite.

*	Étouffant son inquiétude montante, elle va de l’avant à l’aveuglette[.], maintenant une main contre le mur de droite.

-	Par chance, le passage semble filer droit. Au bout d’un moment, ses pas se mettent à résonner plus fortement ; elle se croirait dans un grand hall.

-> hall

= hall // 198 mots.

*	Elle frappe des mains pour évaluer la taille de l’endroit.

	Le claquement se réverbère assez longuement. Où Aoda a-t-elle bien pu atterrir ? Combien de secrets de l’hôtel ignorerait-elle encore ?

	-> hall

*	Elle s’éloigne du mur et continue à avancer.

	Sous ses pas, elle sent des rainures semblant dessiner des motifs étranges. Les lignes semblent aller dans une direction.

	**	Elle décide de suivre l’entrelacs.
		-> centre

	**	Elle ignore le dessin et continue de chercher une issue.

		Gardant son cap, elle finit par atteindre ce qui semble être un étroit couloir. Elle s’y engouffre, sans vouloir savoir s’il y a d’autres passages. Seul trouver une sortie compte.

		-> sortie

*	Elle continue de longer le mur.

	Sous ses doigts, elle sent comme des bas-reliefs. À moins que son imagination lui joue des tours et qu’il ne s’agit que des aspérités de la paroi.

	**	Du bout des doigts, elle cherche à savoir ce qui est représenté sur le mur.
		-> motifs

	**	C’est ça, il ne s’agit que de son imagination et elle préfère chercher une sortie.

		Elle poursuit donc le long du mur jusqu’à atteindre un passage. Elle s’y engouffre, espérant trouver une issue.

		-> sortie

= centre // 181 mots.

Elle atteint ce qui semble être le centre du hall. Là, elle s’accroupit et touche le sol : une forme géométrique complexe y est gravée. Machinalement, sans qu’elle sache exactement ce qui la pousse, Aoda s’assied en tailleur et ferme les yeux.

Une sensation s’insinue en elle, lui hérissant le poil tout d’abord, puis l’apaisant. Elle a l’impression d’être observée, d’être au centre des univers.

« Eh, Aoda, ça va ? »

L’intéressée s’ébroue, hagarde, et regarde autour d’elle. Plusieurs clients l’entourent, l’un d’eux pointant une lampe torche sur elle.

La propriétaire de l’hôtel se passe la main dans ses cheveux, se gratte la tête et remet ses courtes mèches en place. Elle est dans la pièce dont la porte ne s’ouvre plus ! Et la façon dont elle est arrivée ici ne laisse qu’une trace floue dans son esprit.

Se forçant à reprendre son assurance habituelle, elle réagit avec un sourire :

-> fin

= motifs // 130 mots.

Elle palpe le tracé. Ici, elle croit distinguer une jambe, là un bras, et tout en haut, un visage. Qui la regarde, elle en est certaine. Plus loin, une autre figure est gravée dans le mur, et encore une autre, et une autre…

Il lui semble entendre des murmures inintelligibles. Cela aurait dû être effrayant, mais ils la rassurent pourtant, la guident. Continuant sa progression, elle finit par atteindre un couloir, qu’elle emprunte.

Peu de temps plus tard, il débouche sur la pièce où ses clients sont restés bloqués. L’un d’eux braque une lampe torche sur la propriétaire de l’hôtel.

« Aoda ! Comment c’est… Mais… ce passage n’était pas là il y a un instant ! »

Repoussant les interrogations qui commencent à l’assaillir, elle déclare :

-> fin

= sortie // 118 mots.

Après un temps qui lui semble indéterminé, elle parvient à une impasse. Refoulant sa frustration, elle frappe le mur du poing. Sauf qu’il n’y a plus de mur.

Encore son imagination, ou bien la fatigue, ou les ténèbres silencieuses dans lesquelles elle se trouve ?

Le faisceau d’une lampe torche lui fait plisser les yeux.

« Aoda, c’est toi ? Par où… Mais, ce passage n’existait pas il y a quelques instants ! »

Plusieurs clients lui font face, l’air un peu perdu. La propriétaire de l’hôtel n’y comprend rien non plus et essaie de se donner une contenance :

« Vous devriez chercher un peu mieux la prochaine fois… »

Puis elle ajoute, devant les mines mécontentes :

-> fin

=== fin === // 175 mots.

« Venez, j’ai trouvé une autre issue. À moins que vous commencez à vous plaire ici ? »

Toute désorientation l’ayant quittée, Aoda mène le petit groupe. Tout a disparu, le hall, le sombre passage aux murs de pierre, l’escalier.

Il ne reste qu’un simple couloir, qui aboutit à un autre couloir qu’elle connaît bien, dans l’hôtel. Derrière la troupe, il n’y a plus qu’un papier peint de couleur rouge.

« Aoda, t’y comprends quelque chose ? »

Elle ne répond pas immédiatement. Quand enfin elle prend la parole :

« Allez vous reposer. Vous retournerez dans votre monde dès que possible, quand j’aurai résolu le problème de la serrure brisée. Quant à ce qu’il vient de nous arriver, oublions : l’hôtel doit nous réserver encore bien des secrets. »

Les autres acquiescent en silence.

Dès qu’elle le pourra, Aoda en parlera avec Céleste. Tout cela n’est pas normal et elle craint le pire. Le problème est bien plus grave qu’il n’en a l’air, elle en est convaincue.

{introduction.ignorer.encore > 10: -> surprise}

-> END

=== surprise === // 97 mots.

/* Un petit secret si on ignore assez longtemps au début. ;) */

Elle retourne à son bureau. Devant sur le seuil, le client qui l’a avertie au tout début de cette aventure l’interpelle.

« T’en as mis du temps, dis donc ! Si tu ne m’avais pas ignoré aussi longtemps tout à l’heure… Enfin bref. Céleste vient de passer, tu la rates de peu. Elle avait l’air préoccupée et t’a laissé un message. »

Aoda arrache vivement le papier des mains de son interlocuteur et le déplie.

<i>Retrouve-moi dans la forêt derrière l’hôtel, tu sais où. J’ai découvert des choses inquiétantes.</i>

<i>Céleste</i>

-> END
